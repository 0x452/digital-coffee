package com.digital.coffee.shops.ui.converter

import com.digital.coffee.core.utils.ResourceService
import com.digital.coffee.shops.R
import com.digital.coffee.shops.mock.MenuModelToUiModelConverterMock
import com.digital.coffee.shops.ui.converter.menu.MenuModelToUiModelConverter
import com.nhaarman.mockitokotlin2.*
import org.junit.Assert.assertEquals
import org.junit.Test

class MenuModelToUiModelConverterTest {

    private val resourceService: ResourceService = mock()

    private val uiConverter = MenuModelToUiModelConverter(
        resourceService = resourceService
    )

    @Test
    fun convert() {
        // mock
        val models = MenuModelToUiModelConverterMock.models
        val uiModels = MenuModelToUiModelConverterMock.uiModels

        whenever(resourceService.getString(R.string.shops_menu_item_price, 100))
            .thenReturn("100 ₽")

        // action
        val result = uiConverter.convert(models)

        // verify
        assertEquals(uiModels, result)
    }
}