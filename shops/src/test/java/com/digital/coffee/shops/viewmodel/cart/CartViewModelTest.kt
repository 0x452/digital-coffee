package com.digital.coffee.shops.viewmodel.cart

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.digital.coffee.core.utils.SharedPref
import com.digital.coffee.core_tests.TestCoroutineRule
import com.digital.coffee.shops.data.model.response.OrderResponseModel
import com.digital.coffee.shops.data.repository.orders.IOrdersRepository
import com.digital.coffee.shops.mock.OrderMock
import com.digital.coffee.shops.data.adapter.ICartAdapter
import com.digital.coffee.shops.ui.eventbus.ICoffeeshopEventBus
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.flow.flowOf
import org.junit.After
import org.junit.Rule
import org.junit.Test

class CartViewModelTest {

    private val ordersRepository: IOrdersRepository = mock()
    private val cartAdapter: ICartAdapter = mock()
    private val coffeeshopEventBus: ICoffeeshopEventBus = mock()
    private val sharedPref: SharedPref = mock()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val viewModel = CartViewModel(
        repository = ordersRepository,
        cartAdapter = cartAdapter,
        coffeeshopEventBus = coffeeshopEventBus,
        sharedPref = sharedPref
    )

    @After
    fun tearDown() {
        verifyNoMoreInteractions(coffeeshopEventBus, ordersRepository, cartAdapter)
    }

    @Test
    fun sendOrder() = testCoroutineRule.runTest {
        // mock
        val order = OrderMock.uiModel.order
        val coffeeshopId = 1
        val token = "test"

        val response = OrderResponseModel(1)

        whenever(sharedPref.getFirebaseToken())
            .thenReturn(token)
        whenever(ordersRepository.sendOrder(coffeeshopId, token, order))
            .thenReturn(flowOf(response))

        // action
        viewModel.sendOrder(coffeeshopId, order)

        // verify
        verify(ordersRepository).sendOrder(coffeeshopId, token, order)
    }

    @Test
    fun clearItems() {
        // action
        viewModel.clearItems()

        // verify
        verify(cartAdapter).clear()
    }
//    TODO: replace with coroutines tests
//    @Test
//    fun postOrderId() {
//        // mock
//        val id = 1
//
//        // action
//        viewModel.postOrderId(id)
//
//        // verify
//        verify(coffeeshopEventBus).postOrderId(id)
//    }

    @Test
    fun getItems() {
        // action
        viewModel.getCartItems()

        // verify
        verify(cartAdapter).getItems()
    }
}