package com.digital.coffee.shops.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.digital.coffee.core_tests.TestCoroutineRule
import com.digital.coffee.shops.data.CoffeeshopsService
import com.digital.coffee.shops.data.model.response.CoffeeshopInfoModel
import com.digital.coffee.shops.data.repository.CoffeeshopsRepository
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.first
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class CoffeeshopsRepositoryTest {

    private val service: CoffeeshopsService = mock()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val repository = CoffeeshopsRepository(
        service = service,
    )

    @After
    fun tearDown() {
        verifyNoMoreInteractions(service)
    }

    @Test
    fun getCoffeeshopInfo() = testCoroutineRule.runTest {
        // mock
        val response = CoffeeshopInfoModel(
            title = "test",
            address = "test",
            products = listOf()
        )

        whenever(service.getCoffeeshopInfo(1))
            .thenReturn(response)

        // action
        val result = repository.getCoffeeshopInfo(1).first()

        // verify
        verify(service).getCoffeeshopInfo(1)
        assertEquals(response, result)
    }
}