package com.digital.coffee.shops.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.digital.coffee.core.utils.SharedPref
import com.digital.coffee.core_tests.TestCoroutineRule
import com.digital.coffee.shops.data.model.response.CoffeeshopInfoModel
import com.digital.coffee.shops.data.repository.CoffeeshopsRepository
import com.digital.coffee.shops.mock.MenuDetailsMock
import com.digital.coffee.shops.data.adapter.ICartAdapter
import com.digital.coffee.shops.ui.converter.menu.IMenuModelToUiModelConverter
import com.digital.coffee.shops.ui.eventbus.ICoffeeshopEventBus
import com.digital.coffee.shops.ui.eventbus.IMenuDetailsEventBus
import com.digital.coffee.shops.viewmodel.coffeeshops.CoffeeshopsViewModel
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.flowOf
import org.junit.After
import org.junit.Rule
import org.junit.Test

class CoffeeshopsViewModelTest {

    private val repository: CoffeeshopsRepository = mock()
    private val eventBus: IMenuDetailsEventBus = mock()
    private val uiModelConverter: IMenuModelToUiModelConverter = mock()
    private val sharedPref: SharedPref = mock()
    private val coffeeshopsEventBus: ICoffeeshopEventBus = mock()
    private val cartAdapter: ICartAdapter = mock()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val viewModel = CoffeeshopsViewModel(
        repository = repository,
        uiModelConverter = uiModelConverter,
        menuDetailsEventBus = eventBus,
        coffeeshopEventBus = coffeeshopsEventBus,
        cartAdapter = cartAdapter
    )

    @After
    fun tearDown() {
        verifyNoMoreInteractions(repository, sharedPref, uiModelConverter)
    }

//    @Test
//    fun getCoffeeshopInfo() = testCoroutineRule.runTest {
//        // mock
//        val response = CoffeeshopInfoModel(
//            title = "test",
//            address = "test",
//            products = listOf()
//        )
//
//        whenever(repository.getCoffeeshopInfo(1))
//            .thenReturn(flowOf(response))
//
//        // action
//        viewModel.getCoffeeshopInfo(1)
//
//        // verify
//        verify(repository).getCoffeeshopInfo(1)
//        verify(uiModelConverter).convert(response.products)
//    }

//    TODO: replace with coroutines tests
//    @Test
//    fun postMenuDetailsItem() {
//        // mock
//        val detail = MenuDetailsMock.uiModel
//
//        // action
//        viewModel.postMenuDetailsItem(detail)
//
//        // verify
//        verify(eventBus).postMenuItemDetails(detail)
//    }
//
//    @Test
//    fun postCoffeeshopId() {
//        // mock
//        val id = 1
//
//        // action
//        viewModel.postCoffeeshopId(id)
//
//        // verify
//        verify(coffeeshopsEventBus).postCoffeeshopId(id)
//    }

//    @Test
//    fun addItem() {
//        // mock
//        val item = MenuDetailsMock.uiModel
//
//        // action
//        viewModel.addItem(item)
//
//        //verify
//        verify(cartAdapter).add(item)
//    }
}