package com.digital.coffee.shops.ui.converter

import com.digital.coffee.shops.mock.MenuModelToCartModelConverterMock
import com.digital.coffee.shops.ui.converter.cart.MenuModelToCartModelConverter
import org.junit.Assert.assertEquals
import org.junit.Test

class MenuModelToCartModelConverterTest {

    private val converter = MenuModelToCartModelConverter()

    @Test
    fun convert() {
        // mock
        val item = MenuModelToCartModelConverterMock.model
        val uiModel = MenuModelToCartModelConverterMock.uiModel

        // action
        val result = converter.convert(item)

        // mock
        assertEquals(result, uiModel)
    }
}