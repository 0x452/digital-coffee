package com.digital.coffee.shops.mock

import com.digital.coffee.shops.data.model.response.MenuItemModel
import com.digital.coffee.shops.ui.model.MenuItemUiModel

object MenuModelToUiModelConverterMock {
    val models = listOf(
        MenuItemModel(
            title = "test",
            type = "type",
            price = 100,
            url = "https://image-url.com"
        )
    )

    val uiModels = listOf(
        MenuItemUiModel(
            title = "test",
            type = "type",
            price = "100 ₽",
        )
    )
}