package com.digital.coffee.shops.mock

import com.digital.coffee.shops.data.model.request.OrderRequestModel
import com.digital.coffee.shops.ui.model.CartItemUiModel

object OrderMock {

    val cartItem = CartItemUiModel(
        title = "Капучино",
        amount = 2,
        price = 120,
    )

    val uiModel = OrderRequestModel(
        token = "test",
        order = listOf(cartItem)
    )
}