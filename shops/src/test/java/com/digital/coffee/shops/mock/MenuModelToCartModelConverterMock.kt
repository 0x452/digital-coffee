package com.digital.coffee.shops.mock

import com.digital.coffee.shops.ui.model.CartItemUiModel
import com.digital.coffee.shops.ui.model.MenuItemUiModel

object MenuModelToCartModelConverterMock {

    val model = MenuItemUiModel(
        title = "Капучино",
        price = "100 ₽",
        type = "DRINKS"
    )

    val uiModel = CartItemUiModel(
        title = "Капучино",
        price = 100,
        amount = 1
    )
}