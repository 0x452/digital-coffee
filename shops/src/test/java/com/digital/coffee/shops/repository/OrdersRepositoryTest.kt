package com.digital.coffee.shops.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.digital.coffee.core_tests.TestCoroutineRule
import com.digital.coffee.shops.data.OrdersService
import com.digital.coffee.shops.data.model.request.OrderRequestModel
import com.digital.coffee.shops.data.repository.orders.OrdersRepository
import com.digital.coffee.shops.mock.OrderMock
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.flow.first
import org.junit.After
import org.junit.Rule
import org.junit.Test

class OrdersRepositoryTest {

    private val service: OrdersService = mock()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val repository = OrdersRepository(
        service = service,
    )

    @After
    fun tearDown() {
        verifyNoMoreInteractions(service)
    }

//    @Test
//    fun sendOrder() = testCoroutineRule.runTest {
//        // mock
//        val order = OrderMock.uiModel
//        val coffeeshopId = 1
//        val token = "test"
//
//        whenever(service.sendOrder(coffeeshopId, OrderRequestModel(token, order.order)))
//            .thenReturn()
//
//        // action
//        val result = repository.sendOrder(coffeeshopId, token, order.order).first()
//
//        // verify
//        verify(service).sendOrder(coffeeshopId, order)
//        assertEquals()
//    }
}