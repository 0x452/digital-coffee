package com.digital.coffee.shops.mock

import com.digital.coffee.shops.ui.model.MenuItemUiModel

object MenuDetailsMock {

    val uiModel = MenuItemUiModel(
        title = "Капучино",
        price = "100 ₽",
        type = "DRINKS"
    )
}