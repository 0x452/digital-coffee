package com.digital.coffee.shops.di.order

import com.digital.coffee.shops.di.order.module.OrderDetailsBindModule
import com.digital.coffee.shops.di.order.module.OrderDetailsModule
import com.digital.coffee.shops.di.order.scope.OrderDetailsScope
import com.digital.coffee.shops.ui.fragment.OrderDetailsFragment
import dagger.Subcomponent

@OrderDetailsScope
@Subcomponent(
    modules = [
        OrderDetailsBindModule::class,
        OrderDetailsModule::class
    ]
)

internal interface OrderDetailsComponent {
    fun inject(fragment: OrderDetailsFragment)
}