package com.digital.coffee.shops.viewmodel.coffeeshops

import com.digital.coffee.core.utils.flow.RemoteData
import com.digital.coffee.shops.ui.model.MenuItemUiModel

data class CoffeeshopsViewState(
    val menuItems: RemoteData<List<MenuItemUiModel>> = RemoteData.Initial
)