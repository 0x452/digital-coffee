package com.digital.coffee.shops.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.digital.coffee.shops.R
import com.digital.coffee.shops.databinding.ItemMenuBinding
import com.digital.coffee.shops.ui.model.MenuItemUiModel

class MenuAdapter(
    private val onItemClick: (MenuItemUiModel) -> Unit,
    private val onAddToCartClick: (MenuItemUiModel) -> Unit
): RecyclerView.Adapter<MenuViewHolder>() {
    private var items: List<MenuItemUiModel> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        val binding: ItemMenuBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            MenuViewHolder.LAYOUT,
            parent,
            false
        )

        return MenuViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        holder.dataBinding.also {
            it.tvTitle.text = items[position].title
            it.tvPrice.text = items[position].price
            it.btnAddToCart.setOnClickListener { onAddToCartClick(items[position]) }
            it.root.setOnClickListener { onItemClick(items[position]) }
        }
    }

    override fun getItemCount(): Int = items.size

    fun setAdapterItems(items: List<MenuItemUiModel>) {
        this.items = items
    }
}

class MenuViewHolder(val dataBinding: ItemMenuBinding) :
    RecyclerView.ViewHolder(dataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.item_menu
    }
}