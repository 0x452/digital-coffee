package com.digital.coffee.shops.data.model.response

data class MenuItemModel(
    val title: String,
    val price: Int,
    val url: String,
    val type: String
)