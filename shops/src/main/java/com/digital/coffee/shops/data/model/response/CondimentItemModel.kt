package com.digital.coffee.shops.data.model.response

data class CondimentItemModel(
    val title: String,
    val price: Int,
    val url: String,
    val amount: Int
)
