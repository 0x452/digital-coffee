package com.digital.coffee.shops.viewmodel.coffeeshops

import androidx.lifecycle.viewModelScope
import com.digital.coffee.core.utils.flow.RemoteData
import com.digital.coffee.core.utils.viewmodel.BaseViewModel
import com.digital.coffee.shops.data.adapter.ICartAdapter
import com.digital.coffee.shops.data.repository.ICoffeeshopsRepository
import com.digital.coffee.shops.di.coffeeshops.scope.CoffeeshopsScope
import com.digital.coffee.shops.ui.converter.menu.IMenuModelToUiModelConverter
import com.digital.coffee.shops.ui.eventbus.ICoffeeshopEventBus
import com.digital.coffee.shops.ui.eventbus.IMenuDetailsEventBus
import com.digital.coffee.shops.ui.model.MenuItemUiModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@CoffeeshopsScope
class CoffeeshopsViewModel @Inject constructor(
    private val repository: ICoffeeshopsRepository,
    private val menuDetailsEventBus: IMenuDetailsEventBus,
    private val coffeeshopEventBus: ICoffeeshopEventBus,
    private val uiModelConverter: IMenuModelToUiModelConverter,
    private val cartAdapter: ICartAdapter
) : BaseViewModel<CoffeeshopsViewState, CoffeeshopsEvent>() {

    override val initalState: CoffeeshopsViewState = CoffeeshopsViewState()

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        _viewState.update { state ->
            state.copy(
                menuItems = RemoteData.Failure(Exception(throwable))
            )
        }
    }

    init {
        startListenOrderId()
    }

    private fun startListenOrderId() {
        viewModelScope.launch {
            coffeeshopEventBus.orderIdSubject
                .collect {
                    postEvent(CoffeeshopsEvent.NavigateToOrderDetails(it))
                }
        }
    }

    fun getCoffeeshopInfo(id: Int) =
        viewModelScope.launch(exceptionHandler) {
            repository.getCoffeeshopInfo(id)
                .onStart {
                    _viewState.update { state ->
                        state.copy(
                            menuItems = RemoteData.Loading
                        )
                    }
                }
                .map { uiModelConverter.convert(it.products) }
                .collect {
                    _viewState.update { state ->
                        state.copy(
                            menuItems = RemoteData.Success(it)
                        )
                    }
                }
            coffeeshopEventBus.postCoffeeshopId(id)
        }

    fun postMenuDetailsItem(item: MenuItemUiModel) {
        viewModelScope.launch {
            menuDetailsEventBus.postMenuItemDetails(item)
        }
    }

    fun postCoffeeshopId(coffeeshopId: Int) {
        viewModelScope.launch {
            coffeeshopEventBus.postCoffeeshopId(coffeeshopId)
        }
    }

    fun addItem(item: MenuItemUiModel) {
        cartAdapter.add(item)
    }

}