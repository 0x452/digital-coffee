package com.digital.coffee.shops.viewmodel.details

sealed class MenuDetailsEvent {
    object SetMenuDetails: MenuDetailsEvent()
}
