package com.digital.coffee.shops.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.digital.coffee.core.utils.binding.viewBinding
import com.digital.coffee.core.utils.flow.RemoteData
import com.digital.coffee.core.utils.fragment.BaseBottomSheetFragment
import com.digital.coffee.core.utils.fragment.observeFlow
import com.digital.coffee.shops.R
import com.digital.coffee.shops.databinding.FmtCartBinding
import com.digital.coffee.shops.di.ComponentManager
import com.digital.coffee.shops.ui.adapter.CartAdapter
import com.digital.coffee.shops.viewmodel.cart.CartEvent
import com.digital.coffee.shops.viewmodel.cart.CartViewModel
import com.digital.coffee.shops.viewmodel.cart.CartViewState

class CartFragment : BaseBottomSheetFragment(R.layout.fmt_cart) {

    private val viewModel: CartViewModel by viewModels { viewModelFactory }
    private val adapter: CartAdapter by lazy { CartAdapter() }

    private val viewBinding: FmtCartBinding by viewBinding(FmtCartBinding::bind)

    override fun onInitDependencyInjection() {
        ComponentManager.plusCartComponent().inject(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        ComponentManager.clearCartComponent()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter.setAdapterItems(viewModel.getCartItems())

        viewBinding.rvCart.adapter = adapter
        viewBinding.rvCart.layoutManager = LinearLayoutManager(requireContext())

        viewBinding.btnGooglePay.setOnClickListener {
            viewModel.sendOrderToCoffeeshop()
        }

        observeFlow { viewModel.events.collect(::onEvent) }
        observeFlow { viewModel.viewState.collect(::updateUiState) }
    }

    private fun onEvent(event: CartEvent) {
        when (event) {
            is CartEvent.SendOrderToCoffeeshop -> {
                viewModel.sendOrder(event.id, viewModel.getCartItems())
            }
        }
    }

    private fun updateUiState(state: CartViewState) {
        when (state.order) {
            is RemoteData.Initial, RemoteData.Loading -> {}
            is RemoteData.Success -> {
                viewModel.postOrderId(state.order.value.orderId)
                viewModel.clearItems()
                dismiss()
            }
            is RemoteData.Failure -> {}
        }
    }
}