package com.digital.coffee.shops.data.repository.orders

import com.digital.coffee.core.utils.coroutines.Dispatcher
import com.digital.coffee.shops.data.OrdersService
import com.digital.coffee.shops.data.model.request.OrderRequestModel
import com.digital.coffee.shops.data.model.response.OrderResponseModel
import com.digital.coffee.shops.ui.model.CartItemUiModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class OrdersRepository @Inject constructor(
    private val service: OrdersService
): IOrdersRepository {

    override fun sendOrder(coffeeshopId: Int, token: String?, order: List<CartItemUiModel>): Flow<OrderResponseModel> =
        flow {
            emit(service.sendOrder(coffeeshopId, OrderRequestModel(token, order = order)))
        }
            .flowOn(Dispatcher.IO)

}