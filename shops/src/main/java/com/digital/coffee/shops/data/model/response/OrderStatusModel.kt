package com.digital.coffee.shops.data.model.response

data class OrderStatusModel(
    val orderId: Int,
    val status: OrderStatus,
    val address: String,
    val items: List<MenuItemModel>
)
