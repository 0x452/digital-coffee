package com.digital.coffee.shops.di.cart

import com.digital.coffee.shops.di.cart.module.CartBindModule
import com.digital.coffee.shops.di.cart.module.CartModule
import com.digital.coffee.shops.di.cart.scope.CartScope
import com.digital.coffee.shops.ui.fragment.CartFragment
import dagger.Subcomponent

@CartScope
@Subcomponent(
    modules = [
        CartModule::class,
        CartBindModule::class
    ]
)

internal interface CartComponent {
    fun inject(fragment: CartFragment)
}