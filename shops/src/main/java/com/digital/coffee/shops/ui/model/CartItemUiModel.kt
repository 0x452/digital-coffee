package com.digital.coffee.shops.ui.model

data class CartItemUiModel(
    val title: String,
    val price: Int,
    var amount: Int
)
