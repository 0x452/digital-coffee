package com.digital.coffee.shops.data.repository

import com.digital.coffee.shops.data.model.response.CoffeeshopInfoModel
import kotlinx.coroutines.flow.Flow

interface ICoffeeshopsRepository {

    fun getCoffeeshopInfo(id: Int): Flow<CoffeeshopInfoModel>

}