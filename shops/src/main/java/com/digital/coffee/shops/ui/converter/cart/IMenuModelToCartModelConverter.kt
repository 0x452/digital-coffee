package com.digital.coffee.shops.ui.converter.cart

import com.digital.coffee.shops.ui.model.CartItemUiModel
import com.digital.coffee.shops.ui.model.MenuItemUiModel

interface IMenuModelToCartModelConverter {

    fun convert(item: MenuItemUiModel): CartItemUiModel

}