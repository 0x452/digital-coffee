package com.digital.coffee.shops.di.details.module

import androidx.lifecycle.ViewModel
import com.digital.coffee.core.di.module.ViewModelFactoryModule
import com.digital.coffee.core.utils.viewmodel.ViewModelKey
import com.digital.coffee.shops.viewmodel.details.MenuDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(
    includes = [
        ViewModelFactoryModule::class
    ]
)

abstract class MenuDetailsBindModule {

    @Binds
    @IntoMap
    @ViewModelKey(MenuDetailsViewModel::class)
    abstract fun bindMenuDetailsViewModel(
        menuDetailsViewModel: MenuDetailsViewModel
    ): ViewModel
}