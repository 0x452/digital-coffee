package com.digital.coffee.shops.ui.eventbus

import com.digital.coffee.shops.ui.model.MenuItemUiModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import javax.inject.Inject

class MenuDetailsEventBus @Inject constructor(): IMenuDetailsEventBus {

    private val _menuDetailsSubject = MutableSharedFlow<MenuItemUiModel>(replay = 1)

    override val menuDetailsSubject
        get() = _menuDetailsSubject.asSharedFlow()

    override suspend fun postMenuItemDetails(item: MenuItemUiModel) {
        _menuDetailsSubject.emit(item)
    }

}