package com.digital.coffee.shops.data.repository

import android.content.Context
import android.location.Address
import android.location.Geocoder
import com.digital.coffee.shops.di.coffeeshops.scope.CoffeeshopsScope
import javax.inject.Inject

@CoffeeshopsScope
class GeocoderRepository @Inject constructor(
    private val context: Context
) : IGeocoderRepository {

    private companion object {
        const val MAX_ADDRESS = 2
    }

    override fun getCoords(address: String): List<Address> =
        Geocoder(context)
            .getFromLocationName(address, MAX_ADDRESS)

}