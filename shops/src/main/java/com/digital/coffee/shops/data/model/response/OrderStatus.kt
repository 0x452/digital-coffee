package com.digital.coffee.shops.data.model.response

enum class OrderStatus {
    ACCEPTED, IN_PROCESS, READY
}