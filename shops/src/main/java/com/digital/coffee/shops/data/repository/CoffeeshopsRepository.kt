package com.digital.coffee.shops.data.repository

import com.digital.coffee.core.utils.coroutines.Dispatcher
import com.digital.coffee.shops.data.CoffeeshopsService
import com.digital.coffee.shops.data.model.response.CoffeeshopInfoModel
import com.digital.coffee.shops.di.coffeeshops.scope.CoffeeshopsScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@CoffeeshopsScope
class CoffeeshopsRepository @Inject constructor(
    private val service: CoffeeshopsService
) : ICoffeeshopsRepository {

    override fun getCoffeeshopInfo(id: Int): Flow<CoffeeshopInfoModel> =
        flow {
            emit(service.getCoffeeshopInfo(id))
        }
            .flowOn(Dispatcher.IO)

}