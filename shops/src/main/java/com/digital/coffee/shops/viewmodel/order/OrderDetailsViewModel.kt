package com.digital.coffee.shops.viewmodel.order

import androidx.lifecycle.viewModelScope
import com.digital.coffee.core.utils.flow.RemoteData
import com.digital.coffee.core.utils.viewmodel.BaseViewModel
import com.digital.coffee.shops.data.repository.IGeocoderRepository
import com.digital.coffee.shops.data.repository.orders.details.IOrderDetailsRepository
import com.digital.coffee.shops.di.order.scope.OrderDetailsScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@OrderDetailsScope
class OrderDetailsViewModel @Inject constructor(
    private val repository: IOrderDetailsRepository,
    private val geocoderRepository: IGeocoderRepository
) : BaseViewModel<OrderDetailsViewState, OrderDetailsEvent>() {

    override val initalState: OrderDetailsViewState = OrderDetailsViewState()

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        _viewState.update { state ->
            state.copy(
                orderDetails = RemoteData.Failure(Exception(throwable))
            )
        }
    }

    fun getOrderDetails(coffeeshopId: Int, orderId: Int) {
        viewModelScope.launch(exceptionHandler) {
            repository.getOrderDetails(coffeeshopId, orderId)
                .onStart {
                    _viewState.update { state ->
                        state.copy(
                            orderDetails = RemoteData.Loading
                        )
                    }
                }
                .collect {
                    _viewState.update { state ->
                        state.copy(
                            orderDetails = RemoteData.Success(it)
                        )
                    }
                }
        }
    }

    fun findCoffeeshopCoords(address: String) {
        val geocoder = geocoderRepository.getCoords(address)

        if (geocoder.isNotEmpty()) {
            postEvent(OrderDetailsEvent.DrawAddressPoint(geocoder.first()))
        }
    }
}