package com.digital.coffee.shops.viewmodel.details

import com.digital.coffee.shops.ui.model.MenuItemUiModel

data class MenuDetailsViewState(
    val menuItem: MenuItemUiModel? = null
)
