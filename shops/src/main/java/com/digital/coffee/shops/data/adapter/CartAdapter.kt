package com.digital.coffee.shops.data.adapter

import com.digital.coffee.shops.ui.converter.cart.IMenuModelToCartModelConverter
import com.digital.coffee.shops.ui.model.CartItemUiModel
import com.digital.coffee.shops.ui.model.MenuItemUiModel
import javax.inject.Inject

class CartAdapter @Inject constructor(
    private val converter: IMenuModelToCartModelConverter
): ICartAdapter {

    private val cart: MutableList<CartItemUiModel> = mutableListOf()

    private companion object {
        const val INVALID_INDEX = -1
    }

    override fun add(item: MenuItemUiModel) {
        val index = cart.indexOfFirst { it.title == item.title }
        if (index == INVALID_INDEX) {
            cart.add(converter.convert(item))
        } else {
            cart[index].amount += 1
        }
    }

    override fun getItems() = cart.toList()

    override fun delete(item: MenuItemUiModel) {
        cart.removeAt(cart.indexOfFirst { it.title == item.title })
    }

    override fun clear() {
        cart.clear()
    }

}