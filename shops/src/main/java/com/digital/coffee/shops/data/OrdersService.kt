package com.digital.coffee.shops.data

import com.digital.coffee.shops.data.model.request.OrderRequestModel
import com.digital.coffee.shops.data.model.response.OrderResponseModel
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

interface OrdersService {

    @POST("/coffeeshops/{coffeeshopId}/order")
    suspend fun sendOrder(
        @Path("coffeeshopId") coffeeshopId: Int,
        @Body body: OrderRequestModel
    ): OrderResponseModel
}