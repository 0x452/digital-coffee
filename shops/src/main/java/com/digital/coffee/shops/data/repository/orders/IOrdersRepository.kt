package com.digital.coffee.shops.data.repository.orders

import com.digital.coffee.shops.data.model.response.OrderResponseModel
import com.digital.coffee.shops.ui.model.CartItemUiModel
import kotlinx.coroutines.flow.Flow

interface IOrdersRepository {

    fun sendOrder(
        coffeeshopId: Int,
        token: String?,
        order: List<CartItemUiModel>
    ): Flow<OrderResponseModel>
}