package com.digital.coffee.shops.ui.eventbus

import kotlinx.coroutines.flow.SharedFlow

interface ICoffeeshopEventBus {

    val coffeshopIdSubject: SharedFlow<Int>

    val orderIdSubject: SharedFlow<Int>

    suspend fun postCoffeeshopId(coffeeshopId: Int)

    suspend fun postOrderId(orderId: Int)

}