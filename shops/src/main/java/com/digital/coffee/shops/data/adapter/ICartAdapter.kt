package com.digital.coffee.shops.data.adapter

import com.digital.coffee.shops.ui.model.CartItemUiModel
import com.digital.coffee.shops.ui.model.MenuItemUiModel

interface ICartAdapter {

    fun add(item: MenuItemUiModel)

    fun getItems(): List<CartItemUiModel>

    fun delete(item: MenuItemUiModel)

    fun clear()

}
