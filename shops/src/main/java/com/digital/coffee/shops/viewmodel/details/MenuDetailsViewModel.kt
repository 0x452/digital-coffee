package com.digital.coffee.shops.viewmodel.details

import androidx.lifecycle.viewModelScope
import com.digital.coffee.core.utils.viewmodel.BaseViewModel
import com.digital.coffee.shops.di.details.scope.MenuDetailsScope
import com.digital.coffee.shops.data.adapter.ICartAdapter
import com.digital.coffee.shops.ui.eventbus.IMenuDetailsEventBus
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@MenuDetailsScope
class MenuDetailsViewModel @Inject constructor(
    private val detailsEventBus: IMenuDetailsEventBus,
    private val cartAdapter: ICartAdapter
) : BaseViewModel<MenuDetailsViewState, MenuDetailsEvent>() {

    override val initalState: MenuDetailsViewState = MenuDetailsViewState()

    init {
        startListenMenuItem()
    }

    private fun startListenMenuItem() {
        viewModelScope.launch {
            detailsEventBus.menuDetailsSubject
                .collect {
                    _viewState.update { state ->
                        state.copy(
                            menuItem = it
                        )
                    }
                    postEvent(MenuDetailsEvent.SetMenuDetails)
                }
        }
    }

    fun addItem() {
        viewState.value.menuItem?.let(cartAdapter::add)
    }
}