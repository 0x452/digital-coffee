package com.digital.coffee.shops.ui.converter.menu

import com.digital.coffee.shops.R
import com.digital.coffee.shops.data.model.response.MenuItemModel
import com.digital.coffee.shops.di.coffeeshops.scope.CoffeeshopsScope
import com.digital.coffee.shops.ui.model.MenuItemUiModel
import com.digital.coffee.core.utils.ResourceService
import javax.inject.Inject

@CoffeeshopsScope
class MenuModelToUiModelConverter @Inject constructor(
    private val resourceService: ResourceService
) : IMenuModelToUiModelConverter {

    override fun convert(items: List<MenuItemModel>): List<MenuItemUiModel> =
        items.map {
            MenuItemUiModel(
                title = it.title,
                price = resourceService.getString(
                    R.string.shops_menu_item_price,
                    it.price
                ),
                type = it.type
            )
        }
}