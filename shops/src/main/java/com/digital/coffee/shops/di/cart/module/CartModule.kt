package com.digital.coffee.shops.di.cart.module

import com.digital.coffee.shops.data.OrdersService
import com.digital.coffee.shops.di.cart.scope.CartScope
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
internal class CartModule {

    @Provides
    @CartScope
    fun provideOrdersService(retrofit: Retrofit) =
        retrofit.create(OrdersService::class.java)
}