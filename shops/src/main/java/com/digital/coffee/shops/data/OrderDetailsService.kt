package com.digital.coffee.shops.data

import com.digital.coffee.shops.data.model.response.OrderStatusModel
import retrofit2.http.GET
import retrofit2.http.Path

interface OrderDetailsService {

    @GET("/coffeeshops/{coffeeshopId}/order/{orderId}/status")
    suspend fun getOrderStatus(
        @Path("coffeeshopId") coffeeshopId: Int,
        @Path("orderId") orderId: Int
    ): OrderStatusModel

}