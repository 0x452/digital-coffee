package com.digital.coffee.shops.data.repository.orders.details

import com.digital.coffee.core.utils.coroutines.Dispatcher
import com.digital.coffee.shops.data.OrderDetailsService
import com.digital.coffee.shops.data.model.response.OrderStatusModel
import com.digital.coffee.shops.di.order.scope.OrderDetailsScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@OrderDetailsScope
class OrderDetailsRepository @Inject constructor(
    private val service: OrderDetailsService
) : IOrderDetailsRepository {

    override fun getOrderDetails(coffeeshopId: Int, orderId: Int): Flow<OrderStatusModel> = flow {
        emit(service.getOrderStatus(coffeeshopId, orderId))
    }
        .flowOn(Dispatcher.IO)

}