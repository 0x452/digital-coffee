package com.digital.coffee.shops.di.coffeeshops.module

import androidx.lifecycle.ViewModel
import com.digital.coffee.core.di.module.ViewModelFactoryModule
import com.digital.coffee.core.utils.viewmodel.ViewModelKey
import com.digital.coffee.shops.data.repository.CoffeeshopsRepository
import com.digital.coffee.shops.data.repository.GeocoderRepository
import com.digital.coffee.shops.data.repository.ICoffeeshopsRepository
import com.digital.coffee.shops.data.repository.IGeocoderRepository
import com.digital.coffee.shops.ui.converter.cart.IMenuModelToCartModelConverter
import com.digital.coffee.shops.ui.converter.cart.MenuModelToCartModelConverter
import com.digital.coffee.shops.ui.converter.menu.IMenuModelToUiModelConverter
import com.digital.coffee.shops.ui.converter.menu.MenuModelToUiModelConverter
import com.digital.coffee.shops.viewmodel.coffeeshops.CoffeeshopsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(
    includes = [
        ViewModelFactoryModule::class
    ]
)

abstract class CoffeeshopsBindModule {

    @Binds
    @IntoMap
    @ViewModelKey(CoffeeshopsViewModel::class)
    abstract fun bindCoffeeshopsViewModel(
        coffeeshopsViewModel: CoffeeshopsViewModel
    ) : ViewModel

    @Binds
    abstract fun bindCoffeeshopsRepository(
        impl: CoffeeshopsRepository
    ): ICoffeeshopsRepository

    @Binds
    abstract fun bindGeocoderRepository(
        impl: GeocoderRepository
    ): IGeocoderRepository

    @Binds
    abstract fun bindMenuModelToUiModelConverter(
        impl: MenuModelToUiModelConverter
    ): IMenuModelToUiModelConverter

    @Binds
    abstract fun bindMenuModelToCartModelConverter(
        impl: MenuModelToCartModelConverter
    ): IMenuModelToCartModelConverter
}