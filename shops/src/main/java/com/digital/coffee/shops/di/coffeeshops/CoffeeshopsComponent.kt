package com.digital.coffee.shops.di.coffeeshops

import com.digital.coffee.shops.di.coffeeshops.module.CoffeeshopsModule
import com.digital.coffee.shops.ui.fragment.CoffeeshopsMenuFragment
import com.digital.coffee.core.di.CoreComponent
import com.digital.coffee.shops.di.cart.CartComponent
import com.digital.coffee.shops.di.details.MenuDetailsComponent
import com.digital.coffee.shops.di.coffeeshops.module.CoffeeshopsBindModule
import com.digital.coffee.shops.di.coffeeshops.scope.CoffeeshopsScope
import com.digital.coffee.shops.di.order.OrderDetailsComponent
import dagger.Component

@Component(
    modules = [
        CoffeeshopsModule::class,
        CoffeeshopsBindModule::class
    ],
    dependencies = [
        CoreComponent::class
    ]
)

@CoffeeshopsScope
internal interface CoffeeshopsComponent {

    fun inject(fragment: CoffeeshopsMenuFragment)

    fun plusMenuDetailsComponent(): MenuDetailsComponent

    fun plusCartComponent(): CartComponent

    fun plusOrderDetailsComponent(): OrderDetailsComponent

}