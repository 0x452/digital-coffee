package com.digital.coffee.shops.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.digital.coffee.core.utils.binding.viewBinding
import com.digital.coffee.core.utils.fragment.BaseBottomSheetFragment
import com.digital.coffee.core.utils.fragment.observeFlow
import com.digital.coffee.shops.R
import com.digital.coffee.shops.databinding.FmtMenuDetailsBinding
import com.digital.coffee.shops.di.ComponentManager
import com.digital.coffee.shops.viewmodel.details.MenuDetailsEvent
import com.digital.coffee.shops.viewmodel.details.MenuDetailsViewModel

class MenuDetailsFragment : BaseBottomSheetFragment(R.layout.fmt_menu_details) {

    private val viewModel: MenuDetailsViewModel by viewModels { viewModelFactory }
    private val viewBinding: FmtMenuDetailsBinding by viewBinding(FmtMenuDetailsBinding::bind)

    override fun onInitDependencyInjection() {
        ComponentManager.plusMenuDetailsComponent().inject(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        ComponentManager.clearMenuDetailsComponent()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding.btnAddToCart.setOnClickListener {
            viewModel.addItem()
            dismiss()
        }

        observeFlow { viewModel.events.collect(::onEvent) }
    }

    private fun onEvent(event: MenuDetailsEvent) {
        when (event) {
            is MenuDetailsEvent.SetMenuDetails -> {
                viewModel.viewState.value.menuItem?.let { item ->
                    viewBinding.tvTitle.text = item.title
                    viewBinding.tvPrice.text = item.price
                }
            }
        }
    }
}