package com.digital.coffee.shops.data.model.request

import com.digital.coffee.shops.ui.model.CartItemUiModel

data class OrderRequestModel(
    val token: String?,
    val order: List<CartItemUiModel>
)
