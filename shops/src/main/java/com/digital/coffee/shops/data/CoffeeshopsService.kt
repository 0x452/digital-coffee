package com.digital.coffee.shops.data

import com.digital.coffee.shops.data.model.response.CoffeeshopInfoModel
import retrofit2.http.GET
import retrofit2.http.Path

interface CoffeeshopsService {

    @GET("/coffeeshops/{coffeeshopId}")
    suspend fun getCoffeeshopInfo(
        @Path("coffeeshopId") coffeeshopId: Int
    ): CoffeeshopInfoModel

}