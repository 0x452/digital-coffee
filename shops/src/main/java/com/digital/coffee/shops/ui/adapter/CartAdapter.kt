package com.digital.coffee.shops.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.digital.coffee.shops.R
import com.digital.coffee.shops.databinding.ItemCartBinding
import com.digital.coffee.shops.ui.model.CartItemUiModel

class CartAdapter: RecyclerView.Adapter<CartViewHolder>() {
    private var items: List<CartItemUiModel> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val binding: ItemCartBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            CartViewHolder.LAYOUT,
            parent,
            false)

        return CartViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.dataBinding.also {
            it.tvTitle.text = items[position].title
            it.tvPrice.text = items[position].price.toString()
            it.tvCartAmount.text = items[position].amount.toString()
        }
    }

    override fun getItemCount(): Int = items.size

    fun setAdapterItems(items: List<CartItemUiModel>) {
        this.items = items
    }
}

class CartViewHolder(val dataBinding: ItemCartBinding) :
    RecyclerView.ViewHolder(dataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.item_cart
    }
}