package com.digital.coffee.shops.di.coffeeshops.scope

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class CoffeeshopsScope