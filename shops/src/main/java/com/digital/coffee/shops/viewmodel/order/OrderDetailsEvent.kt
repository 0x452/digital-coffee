package com.digital.coffee.shops.viewmodel.order

import android.location.Address

sealed class OrderDetailsEvent {
    data class DrawAddressPoint(val address: Address) : OrderDetailsEvent()
}
