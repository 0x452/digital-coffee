package com.digital.coffee.shops.di.cart.module

import androidx.lifecycle.ViewModel
import com.digital.coffee.core.di.module.ViewModelFactoryModule
import com.digital.coffee.core.utils.viewmodel.ViewModelKey
import com.digital.coffee.shops.data.repository.orders.IOrdersRepository
import com.digital.coffee.shops.data.repository.orders.OrdersRepository
import com.digital.coffee.shops.viewmodel.cart.CartViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(
    includes = [
        ViewModelFactoryModule::class
    ]
)

abstract class CartBindModule {

    @Binds
    @IntoMap
    @ViewModelKey(CartViewModel::class)
    abstract fun bindCartViewModel(
        viewModel: CartViewModel
    ): ViewModel


    @Binds
    abstract fun bindOrdersRepository(
        impl: OrdersRepository
    ): IOrdersRepository
}