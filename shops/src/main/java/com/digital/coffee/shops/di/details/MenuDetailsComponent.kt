package com.digital.coffee.shops.di.details

import com.digital.coffee.shops.di.details.module.MenuDetailsBindModule
import com.digital.coffee.shops.di.details.scope.MenuDetailsScope
import com.digital.coffee.shops.ui.fragment.MenuDetailsFragment
import dagger.Subcomponent

@MenuDetailsScope
@Subcomponent(
    modules = [
        MenuDetailsBindModule::class
    ]
)

internal interface MenuDetailsComponent {
    fun inject(fragment: MenuDetailsFragment)
}