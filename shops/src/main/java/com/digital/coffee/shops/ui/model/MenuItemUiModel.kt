package com.digital.coffee.shops.ui.model

data class MenuItemUiModel(
    val title: String,
    val price: String,
    val type: String
)
