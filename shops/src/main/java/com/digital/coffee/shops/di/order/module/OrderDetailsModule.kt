package com.digital.coffee.shops.di.order.module

import com.digital.coffee.shops.data.OrderDetailsService
import com.digital.coffee.shops.di.order.scope.OrderDetailsScope
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
internal class OrderDetailsModule {

    @Provides
    @OrderDetailsScope
    fun provideOrderDetailsService(retrofit: Retrofit)
            = retrofit.create(OrderDetailsService::class.java)

}