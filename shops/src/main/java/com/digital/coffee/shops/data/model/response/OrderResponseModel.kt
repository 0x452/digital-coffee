package com.digital.coffee.shops.data.model.response

data class OrderResponseModel(
    val orderId: Int
)
