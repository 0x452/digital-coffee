package com.digital.coffee.shops.di.order.module

import androidx.lifecycle.ViewModel
import com.digital.coffee.core.di.module.ViewModelFactoryModule
import com.digital.coffee.core.utils.viewmodel.ViewModelKey
import com.digital.coffee.shops.data.repository.orders.details.IOrderDetailsRepository
import com.digital.coffee.shops.data.repository.orders.details.OrderDetailsRepository
import com.digital.coffee.shops.viewmodel.order.OrderDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(
    includes = [
        ViewModelFactoryModule::class
    ]
)
abstract class OrderDetailsBindModule {

    @Binds
    @IntoMap
    @ViewModelKey(OrderDetailsViewModel::class)
    abstract fun bindOrderDetailsViewModel(
        orderDetailsViewModel: OrderDetailsViewModel
    ): ViewModel

    @Binds
    abstract fun bindOrderDetailsRepository(
        orderDetailsRepository: OrderDetailsRepository
    ): IOrderDetailsRepository

}