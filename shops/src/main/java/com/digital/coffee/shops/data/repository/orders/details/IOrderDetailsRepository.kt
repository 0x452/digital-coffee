package com.digital.coffee.shops.data.repository.orders.details

import com.digital.coffee.shops.data.model.response.OrderStatusModel
import kotlinx.coroutines.flow.Flow

interface IOrderDetailsRepository {

    fun getOrderDetails(coffeeshopId: Int, orderId: Int): Flow<OrderStatusModel>

}