package com.digital.coffee.shops.ui.converter.cart

import com.digital.coffee.shops.ui.model.CartItemUiModel
import com.digital.coffee.shops.ui.model.MenuItemUiModel
import javax.inject.Inject

class MenuModelToCartModelConverter @Inject constructor(): IMenuModelToCartModelConverter {

    private companion object {
        const val RUBLE_SYMBOL = " ₽"
    }

    override fun convert(item: MenuItemUiModel): CartItemUiModel =
        CartItemUiModel(
            title = item.title,
            price = convertToInt(item.price),
            amount = 1
        )

    private fun convertToInt(price: String): Int =
        price.replace(RUBLE_SYMBOL, "").toInt()

}