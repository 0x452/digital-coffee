package com.digital.coffee.shops.di

import android.content.Context
import com.digital.coffee.core.utils.di.InjectUtils
import com.digital.coffee.shops.di.cart.CartComponent
import com.digital.coffee.shops.di.coffeeshops.CoffeeshopsComponent
import com.digital.coffee.shops.di.coffeeshops.DaggerCoffeeshopsComponent
import com.digital.coffee.shops.di.details.MenuDetailsComponent
import com.digital.coffee.shops.di.coffeeshops.module.CoffeeshopsModule
import com.digital.coffee.shops.di.order.OrderDetailsComponent

internal object ComponentManager {
    private lateinit var coffeeshopsComponent: CoffeeshopsComponent

    private var menuDetailsComponent: MenuDetailsComponent? = null
    private var cartComponent: CartComponent? = null
    private var orderDetailsComponent: OrderDetailsComponent? = null

    fun injectCoffeeshopsComponent(context: Context): CoffeeshopsComponent =
        DaggerCoffeeshopsComponent
            .builder()
            .coreComponent(InjectUtils.provideCoreComponent(context.applicationContext))
            .coffeeshopsModule(CoffeeshopsModule())
            .build()
            .also { coffeeshopsComponent = it }

    fun plusMenuDetailsComponent(): MenuDetailsComponent =
        menuDetailsComponent ?: coffeeshopsComponent
            .plusMenuDetailsComponent()
            .also { menuDetailsComponent = it }

    fun plusCartComponent(): CartComponent =
        cartComponent ?: coffeeshopsComponent
            .plusCartComponent()
            .also { cartComponent = it }

    fun plusOrderDetailsComponent(): OrderDetailsComponent =
        orderDetailsComponent ?: coffeeshopsComponent
            .plusOrderDetailsComponent()
            .also { orderDetailsComponent = it }

    fun clearMenuDetailsComponent() {
        menuDetailsComponent = null
    }

    fun clearCartComponent() {
        cartComponent = null
    }

    fun clearOrderDetailsComponent() {
        orderDetailsComponent = null
    }
}