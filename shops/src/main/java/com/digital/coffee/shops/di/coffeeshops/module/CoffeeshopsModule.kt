package com.digital.coffee.shops.di.coffeeshops.module

import com.digital.coffee.shops.data.CoffeeshopsService
import com.digital.coffee.shops.di.coffeeshops.scope.CoffeeshopsScope
import com.digital.coffee.shops.data.adapter.CartAdapter
import com.digital.coffee.shops.data.adapter.ICartAdapter
import com.digital.coffee.shops.ui.converter.cart.IMenuModelToCartModelConverter
import com.digital.coffee.shops.ui.eventbus.CoffeeshopEventBus
import com.digital.coffee.shops.ui.eventbus.ICoffeeshopEventBus
import com.digital.coffee.shops.ui.eventbus.IMenuDetailsEventBus
import com.digital.coffee.shops.ui.eventbus.MenuDetailsEventBus
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
internal class CoffeeshopsModule {

    @Provides
    @CoffeeshopsScope
    fun provideCoffeshopsService(retrofit: Retrofit)
        = retrofit.create(CoffeeshopsService::class.java)

    @Provides
    @CoffeeshopsScope
    fun provideCartAdapter(converter: IMenuModelToCartModelConverter): ICartAdapter
        = CartAdapter(converter)

    @Provides
    @CoffeeshopsScope
    fun provideMenuDetailsEventBus(): IMenuDetailsEventBus
        = MenuDetailsEventBus()

    @Provides
    @CoffeeshopsScope
    fun provideCoffeeshopEventBus(): ICoffeeshopEventBus
        = CoffeeshopEventBus()

}