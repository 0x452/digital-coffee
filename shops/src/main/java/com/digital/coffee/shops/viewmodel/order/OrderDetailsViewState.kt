package com.digital.coffee.shops.viewmodel.order

import com.digital.coffee.core.utils.flow.RemoteData
import com.digital.coffee.shops.data.model.response.OrderStatusModel

data class OrderDetailsViewState(
    val orderDetails: RemoteData<OrderStatusModel> = RemoteData.Initial,
)
