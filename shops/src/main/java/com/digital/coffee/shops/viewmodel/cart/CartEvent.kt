package com.digital.coffee.shops.viewmodel.cart

sealed class CartEvent {
    data class SendOrderToCoffeeshop(val id: Int): CartEvent()
}
