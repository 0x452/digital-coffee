package com.digital.coffee.shops.data.model.response

data class CoffeeshopInfoModel(
    val title: String,
    val address: String,
    val products: List<MenuItemModel>
)