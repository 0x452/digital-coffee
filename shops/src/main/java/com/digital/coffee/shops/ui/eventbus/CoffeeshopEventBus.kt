package com.digital.coffee.shops.ui.eventbus

import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import javax.inject.Inject

class CoffeeshopEventBus @Inject constructor(): ICoffeeshopEventBus {

    private val _coffeeshopIdSubject = MutableSharedFlow<Int>(replay = 1)
    private val _orderIdSubject = MutableSharedFlow<Int>(replay = 1)

    override val coffeshopIdSubject
        get() = _coffeeshopIdSubject.asSharedFlow()

    override val orderIdSubject
        get() = _orderIdSubject.asSharedFlow()

    override suspend fun postCoffeeshopId(coffeeshopId: Int) {
        _coffeeshopIdSubject.emit(coffeeshopId)
    }

    override suspend fun postOrderId(orderId: Int) {
        _orderIdSubject.emit(orderId)
    }
}