package com.digital.coffee.shops.viewmodel.coffeeshops

sealed class CoffeeshopsEvent {
    data class NavigateToOrderDetails(val id: Int) : CoffeeshopsEvent()
}