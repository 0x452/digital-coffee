package com.digital.coffee.shops.di.order.scope

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class OrderDetailsScope
