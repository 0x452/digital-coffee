package com.digital.coffee.shops.viewmodel.cart

import com.digital.coffee.core.utils.flow.RemoteData
import com.digital.coffee.shops.data.model.response.OrderResponseModel

data class CartViewState(
    val order: RemoteData<OrderResponseModel> = RemoteData.Initial
)
