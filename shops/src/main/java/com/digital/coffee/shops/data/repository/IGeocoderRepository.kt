package com.digital.coffee.shops.data.repository

import android.location.Address

interface IGeocoderRepository {

    fun getCoords(address: String): List<Address>

}
