package com.digital.coffee.shops.ui.fragment

import android.location.Address
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.digital.coffee.core.utils.BundleKeys.Companion.COFFEESHOP_ID_KEY
import com.digital.coffee.core.utils.BundleKeys.Companion.ORDER_ID_KEY
import com.digital.coffee.core.utils.binding.viewBinding
import com.digital.coffee.core.utils.flow.RemoteData
import com.digital.coffee.core.utils.fragment.BaseFragment
import com.digital.coffee.core.utils.fragment.observeFlow
import com.digital.coffee.shops.R
import com.digital.coffee.shops.data.model.response.OrderStatus
import com.digital.coffee.shops.data.model.response.OrderStatusModel
import com.digital.coffee.shops.databinding.FmtOrderDetailsBinding
import com.digital.coffee.shops.di.ComponentManager
import com.digital.coffee.shops.viewmodel.order.OrderDetailsEvent
import com.digital.coffee.shops.viewmodel.order.OrderDetailsViewModel
import com.digital.coffee.shops.viewmodel.order.OrderDetailsViewState
import com.yandex.mapkit.Animation
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.CameraPosition

class OrderDetailsFragment : BaseFragment(R.layout.fmt_order_details) {

    private val viewModel: OrderDetailsViewModel by viewModels { viewModelFactory }

    private val viewBinding: FmtOrderDetailsBinding by viewBinding(FmtOrderDetailsBinding::bind)

    override fun onInitDependencyInjection() {
        ComponentManager
            .plusOrderDetailsComponent()
            .inject(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        ComponentManager.clearOrderDetailsComponent()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MapKitFactory.initialize(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let { args ->
            viewModel.getOrderDetails(
                args.getInt(COFFEESHOP_ID_KEY),
                args.getInt(ORDER_ID_KEY)
            )
        }

        observeFlow { viewModel.viewState.collect(::updateUiState) }
        observeFlow { viewModel.events.collect(::onEvent) }
    }

    override fun onStart() {
        super.onStart()
        viewBinding.mapView.onStart()
        MapKitFactory.getInstance().onStart()
    }

    override fun onStop() {
        super.onStop()
        viewBinding.mapView.onStop()
        MapKitFactory.getInstance().onStop()
    }

    private fun updateUiState(state: OrderDetailsViewState) {
        when (state.orderDetails) {
            is RemoteData.Initial, RemoteData.Loading -> {}
            is RemoteData.Success -> {
                viewBinding.tvStatus.text = requireContext().getString(
                    R.string.order_details_status,
                    formatOrderStatus(state.orderDetails.value.status)
                )
                viewBinding.tvOrderId.text =
                    requireContext().getString(R.string.order_details_number, state.orderDetails.value.orderId)
                viewBinding.tvAddress.text = state.orderDetails.value.address
                viewModel.findCoffeeshopCoords(state.orderDetails.value.address)
            }
            is RemoteData.Failure -> {}
        }
    }

    private fun onEvent(event: OrderDetailsEvent) {
        when (event) {
            is OrderDetailsEvent.DrawAddressPoint -> {
                viewBinding.mapView.map.move(
                    CameraPosition(Point(event.address.latitude, event.address.longitude), 16f, 0.0f, 0.0f),
                    Animation(Animation.Type.SMOOTH, .7f),
                    null
                )
            }
        }
    }

    private fun formatOrderStatus(status: OrderStatus): String =
        when (status) {
            OrderStatus.ACCEPTED -> requireContext().resources.getString(R.string.order_details_accepted_status)
            OrderStatus.IN_PROCESS -> requireContext().resources.getString(R.string.order_details_in_process_status)
            OrderStatus.READY -> requireContext().resources.getString(R.string.order_details_ready_status)
        }
}