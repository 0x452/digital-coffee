package com.digital.coffee.shops.ui.fragment

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.digital.coffee.core.utils.BundleKeys.Companion.COFFEESHOP_ID_KEY
import com.digital.coffee.core.utils.BundleKeys.Companion.ORDER_ID_KEY
import com.digital.coffee.core.utils.fragment.BaseFragment
import com.digital.coffee.core.utils.fragment.observeFlow
import com.digital.coffee.core.utils.binding.viewBinding
import com.digital.coffee.core.utils.flow.RemoteData
import com.digital.coffee.shops.R
import com.digital.coffee.shops.databinding.FmtCoffeeshopsMenuBinding
import com.digital.coffee.shops.di.ComponentManager
import com.digital.coffee.shops.ui.adapter.MenuAdapter
import com.digital.coffee.shops.ui.model.MenuItemUiModel
import com.digital.coffee.shops.viewmodel.coffeeshops.CoffeeshopsEvent
import com.digital.coffee.shops.viewmodel.coffeeshops.CoffeeshopsViewModel
import com.digital.coffee.shops.viewmodel.coffeeshops.CoffeeshopsViewState
import com.google.android.material.snackbar.Snackbar

internal class CoffeeshopsMenuFragment : BaseFragment(R.layout.fmt_coffeeshops_menu) {

    private val viewModel: CoffeeshopsViewModel by viewModels { viewModelFactory }
    private val viewBinding: FmtCoffeeshopsMenuBinding by viewBinding(FmtCoffeeshopsMenuBinding::bind)

    private val adapter: MenuAdapter by lazy {
        MenuAdapter(::onItemClick, ::onAddToCartClick)
    }

    override fun onInitDependencyInjection() {
        ComponentManager.injectCoffeeshopsComponent(requireContext()).inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        viewBinding.rvMenu.adapter = adapter
        viewBinding.rvMenu.layoutManager = LinearLayoutManager(requireContext())

        arguments?.getInt(COFFEESHOP_ID_KEY)?.let(viewModel::getCoffeeshopInfo)

        observeFlow { viewModel.viewState.collect(::updateUiState) }
        observeFlow { viewModel.events.collect(::onEvent) }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.coffeshops_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.btnCart -> {
                findNavController().navigate(R.id.cartFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    private fun updateUiState(state: CoffeeshopsViewState) {
        when (state.menuItems) {
            is RemoteData.Initial, RemoteData.Loading -> {}
            is RemoteData.Success -> {
                adapter.setAdapterItems(state.menuItems.value)
            }
            is RemoteData.Failure -> {}
        }
    }

    private fun onEvent(event: CoffeeshopsEvent) {
        when (event) {
            is CoffeeshopsEvent.NavigateToOrderDetails -> {
                showSnackBar("Приступаем к Вашему заказу №${event.id}!")
                findNavController().navigate(
                    R.id.orderDetailsFragment, bundleOf(
                        COFFEESHOP_ID_KEY to arguments?.getInt(COFFEESHOP_ID_KEY),
                        ORDER_ID_KEY to event.id
                    )
                )
            }
        }
    }

    private fun onItemClick(item: MenuItemUiModel) {
        viewModel.postMenuDetailsItem(item)
        findNavController().navigate(R.id.menuDetailsFragment)
    }

    private fun onAddToCartClick(item: MenuItemUiModel) {
        viewModel.addItem(item)
    }

    private fun showSnackBar(text: String) {
        Snackbar.make(requireView(), text, Snackbar.LENGTH_SHORT).show()
    }
}