package com.digital.coffee.shops.viewmodel.cart

import androidx.lifecycle.viewModelScope
import com.digital.coffee.core.utils.SharedPref
import com.digital.coffee.core.utils.flow.RemoteData
import com.digital.coffee.core.utils.viewmodel.BaseViewModel
import com.digital.coffee.shops.data.adapter.ICartAdapter
import com.digital.coffee.shops.data.repository.orders.IOrdersRepository
import com.digital.coffee.shops.di.cart.scope.CartScope
import com.digital.coffee.shops.ui.eventbus.ICoffeeshopEventBus
import com.digital.coffee.shops.ui.model.CartItemUiModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@CartScope
class CartViewModel @Inject constructor(
    private val repository: IOrdersRepository,
    private val coffeeshopEventBus: ICoffeeshopEventBus,
    private val cartAdapter: ICartAdapter,
    private val sharedPref: SharedPref
) : BaseViewModel<CartViewState, CartEvent>() {

    override val initalState: CartViewState = CartViewState()

    private val errorHandler = CoroutineExceptionHandler { _, throwable ->
        _viewState.update { state ->
            state.copy(
                order = RemoteData.Failure(Exception(throwable))
            )
        }
    }


    fun sendOrderToCoffeeshop() {
        viewModelScope.launch {
            coffeeshopEventBus.coffeshopIdSubject
                .collect {
                    postEvent(CartEvent.SendOrderToCoffeeshop(it))
                }
        }
    }

    fun getCartItems(): List<CartItemUiModel> =
        cartAdapter.getItems()

    fun sendOrder(coffeeshopId: Int, order: List<CartItemUiModel>) {
        viewModelScope.launch(errorHandler) {
            repository.sendOrder(coffeeshopId, sharedPref.getFirebaseToken(), order)
                .onStart {
                    _viewState.update { state ->
                        state.copy(order = RemoteData.Loading)
                    }
                }
                .collect {
                    _viewState.update { state ->
                        state.copy(order = RemoteData.Success(it))
                    }
                }

        }
    }

    fun clearItems() {
        cartAdapter.clear()
    }

    fun postOrderId(orderId: Int) {
        viewModelScope.launch {
            coffeeshopEventBus.postOrderId(orderId)
        }
    }
}