package com.digital.coffee.shops.ui.eventbus

import com.digital.coffee.shops.ui.model.MenuItemUiModel
import kotlinx.coroutines.flow.SharedFlow

interface IMenuDetailsEventBus {

    val menuDetailsSubject: SharedFlow<MenuItemUiModel>

    suspend fun postMenuItemDetails(item: MenuItemUiModel)
}