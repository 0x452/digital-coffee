package com.digital.coffee.shops.ui.converter.menu

import com.digital.coffee.shops.data.model.response.MenuItemModel
import com.digital.coffee.shops.ui.model.MenuItemUiModel

interface IMenuModelToUiModelConverter {
    fun convert(items: List<MenuItemModel>): List<MenuItemUiModel>
}