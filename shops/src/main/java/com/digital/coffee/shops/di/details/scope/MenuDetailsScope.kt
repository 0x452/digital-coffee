package com.digital.coffee.shops.di.details.scope

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class MenuDetailsScope