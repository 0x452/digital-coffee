package com.digital.coffee.core_tests

import com.digital.coffee.core.utils.coroutines.IDispatcherProvider
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class TestDispatcherProvider: IDispatcherProvider {
    override val IO: CoroutineDispatcher
        get() = Dispatchers.Unconfined

    override val Main: CoroutineDispatcher
        get() = Dispatchers.Main

    override val Default: CoroutineDispatcher
        get() = Dispatchers.Unconfined

    override val Unconfined: CoroutineDispatcher
        get() = Dispatchers.Main
}