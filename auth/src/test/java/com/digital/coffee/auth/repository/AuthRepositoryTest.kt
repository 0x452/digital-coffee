package com.digital.coffee.auth.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.digital.coffee.auth.data.AuthService
import com.digital.coffee.auth.data.model.request.AuthRequestModel
import com.digital.coffee.auth.data.model.response.AuthResponseModel
import com.digital.coffee.auth.data.repository.AuthRepository
import com.digital.coffee.core_tests.TestCoroutineRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.first
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class AuthRepositoryTest {

    private val service: AuthService = mock()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val repository = AuthRepository(
        service = service
    )

    @After
    fun tearDown() {
        verifyNoMoreInteractions(service)
    }

    @Test
    fun authUser() = testCoroutineRule.runTest {
        // mock
        val name = "test"
        val phone = "89881112233"
        val response = AuthResponseModel("token")

        whenever(service.authUser(AuthRequestModel(name, phone)))
            .thenReturn(response)

        // action
        val result = repository.authUser(name, phone).first()

        // verify
        verify(service).authUser(AuthRequestModel(name, phone))
        assertEquals(response, result)
    }
}