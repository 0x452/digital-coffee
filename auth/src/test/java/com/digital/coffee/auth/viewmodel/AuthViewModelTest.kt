package com.digital.coffee.auth.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.digital.coffee.auth.data.model.response.AuthResponseModel
import com.digital.coffee.auth.data.repository.IAuthRepository
import com.digital.coffee.core.ui.permissions.IPermissionHelper
import com.digital.coffee.core.utils.SharedPref
import com.digital.coffee.core_tests.TestCoroutineRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.flowOf
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test

class AuthViewModelTest {

    private val repository: IAuthRepository = mock()
    private val permissionHelper: IPermissionHelper = mock()
    private val sharedPref: SharedPref = mock()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val viewModel = AuthViewModel(
        repository = repository,
        permissionHelper = permissionHelper,
        sharedPref = sharedPref
    )

    @After
    fun tearDown() {
        verifyNoMoreInteractions(repository, permissionHelper)
    }

    @Test
    fun authUser() = testCoroutineRule.runTest {
        // mock
        val name = "test"
        val phone = "89991112233"
        val response = AuthResponseModel("token")

        whenever(repository.authUser(name, phone))
            .thenReturn(flowOf(response))

        // action
        viewModel.authUser(name, phone)

        // verify
        verify(repository).authUser(name, phone)
    }

    @Test
    fun isCameraPermissionGranted() {
        // mock
        whenever(permissionHelper.isPermissionGranted(AuthViewModel.CAMERA_PERMISSION))
            .thenReturn(true)

        // action
        val result = viewModel.isCameraPermissionGranted()

        // verify
        verify(permissionHelper).isPermissionGranted(AuthViewModel.CAMERA_PERMISSION)
        assertTrue(result)
    }
}