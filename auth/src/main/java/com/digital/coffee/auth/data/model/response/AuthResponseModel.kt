package com.digital.coffee.auth.data.model.response

data class AuthResponseModel(
    val token: String
)