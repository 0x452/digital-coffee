package com.digital.coffee.auth.data.repository

import com.digital.coffee.auth.data.model.response.AuthResponseModel
import kotlinx.coroutines.flow.Flow

interface IAuthRepository {

    fun authUser(name: String, phone: String): Flow<AuthResponseModel>

}