package com.digital.coffee.auth.viewmodel

import android.Manifest
import androidx.lifecycle.viewModelScope
import com.digital.coffee.auth.data.repository.IAuthRepository
import com.digital.coffee.auth.di.scope.AuthScope
import com.digital.coffee.core.ui.permissions.IPermissionHelper
import com.digital.coffee.core.utils.SharedPref
import com.digital.coffee.core.utils.flow.RemoteData
import com.digital.coffee.core.utils.viewmodel.BaseViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@AuthScope
class AuthViewModel @Inject constructor(
    private val repository: IAuthRepository,
    private val permissionHelper: IPermissionHelper,
    private val sharedPref: SharedPref
) : BaseViewModel<AuthViewState, AuthEvent>() {

    companion object {
        const val CAMERA_PERMISSION = Manifest.permission.CAMERA
    }

    override val initalState: AuthViewState = AuthViewState()

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        _viewState.update { state ->
            state.copy(
                token = RemoteData.Failure(Exception(throwable))
            )
        }
    }

    fun authUser(name: String, phone: String) {
        viewModelScope.launch(exceptionHandler) {
            repository.authUser(name, phone)
                .onStart {
                    _viewState.update { state ->
                        state.copy(
                            token = RemoteData.Loading
                        )
                    }
                }
                .collect { model ->
                    _viewState.update { state ->
                        state.copy(
                            token = RemoteData.Success(model.token)
                        )
                    }
                }
        }
    }

    fun setUserToken(token: String) {
        sharedPref.setUserToken(token)
    }

    fun onPermissionResult(isGranted: Boolean) {
        postEvent(AuthEvent.CameraPermissionGranted(isGranted))
    }

    fun isCameraPermissionGranted(): Boolean =
        permissionHelper.isPermissionGranted(CAMERA_PERMISSION)
}