package com.digital.coffee.auth.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.digital.coffee.auth.data.AuthService
import com.digital.coffee.auth.data.repository.AuthRepository
import com.digital.coffee.auth.data.repository.IAuthRepository
import com.digital.coffee.auth.di.scope.AuthScope
import com.digital.coffee.auth.viewmodel.AuthViewModel
import com.digital.coffee.core.di.module.ViewModelFactoryModule
import com.digital.coffee.core.utils.viewmodel.ViewModelFactory
import com.digital.coffee.core.utils.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module(
    includes = [
        ViewModelFactoryModule::class
    ]
)

abstract class AuthBindModule {

    @Binds
    @IntoMap
    @ViewModelKey(AuthViewModel::class)
    abstract fun bindAuthViewModel(
        authViewModel: AuthViewModel
    ): ViewModel

    @Binds
    abstract fun bindAuthRepository(
        impl: AuthRepository
    ): IAuthRepository
}