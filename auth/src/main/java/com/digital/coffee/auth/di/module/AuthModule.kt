package com.digital.coffee.auth.di.module

import com.digital.coffee.auth.data.AuthService
import com.digital.coffee.auth.di.scope.AuthScope
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class AuthModule {

    @AuthScope
    @Provides
    fun provideAuthService(retrofit: Retrofit) = retrofit.create(AuthService::class.java)
}