package com.digital.coffee.auth.data.repository

import com.digital.coffee.auth.data.AuthService
import com.digital.coffee.auth.data.model.request.AuthRequestModel
import com.digital.coffee.auth.data.model.response.AuthResponseModel
import com.digital.coffee.auth.di.scope.AuthScope
import com.digital.coffee.core.utils.coroutines.Dispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@AuthScope
class AuthRepository @Inject constructor(
    private val service: AuthService
): IAuthRepository {

    override fun authUser(name: String, phone: String): Flow<AuthResponseModel> =
        flow {
            emit(service.authUser(AuthRequestModel(name, phone)))
        }
            .flowOn(Dispatcher.IO)
}