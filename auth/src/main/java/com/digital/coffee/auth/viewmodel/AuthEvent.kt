package com.digital.coffee.auth.viewmodel

sealed class AuthEvent {
    data class CameraPermissionGranted(val isGranted: Boolean) : AuthEvent()
}