package com.digital.coffee.auth.di

import com.digital.coffee.auth.ui.fragment.AuthFragment
import com.digital.coffee.core.di.CoreComponent
import com.digital.coffee.auth.di.module.AuthModule
import com.digital.coffee.auth.di.module.AuthBindModule
import com.digital.coffee.auth.di.scope.AuthScope
import dagger.Component

@Component(
    modules = [
        AuthModule::class,
        AuthBindModule::class
    ],
    dependencies = [
        CoreComponent::class
    ]
)

@AuthScope
internal interface AuthComponent {
    fun inject(authFragment: AuthFragment)
}