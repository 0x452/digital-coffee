package com.digital.coffee.auth.data

import com.digital.coffee.auth.data.model.request.AuthRequestModel
import com.digital.coffee.auth.data.model.response.AuthResponseModel
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {

    @POST("/users/auth")
    suspend fun authUser(
        @Body body: AuthRequestModel
    ): AuthResponseModel
}