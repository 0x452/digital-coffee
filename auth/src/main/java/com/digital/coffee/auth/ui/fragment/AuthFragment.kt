package com.digital.coffee.auth.ui.fragment

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.digital.coffee.auth.R
import com.digital.coffee.auth.databinding.FmtAuthBinding
import com.digital.coffee.auth.di.DaggerAuthComponent
import com.digital.coffee.auth.di.module.AuthModule
import com.digital.coffee.auth.viewmodel.AuthEvent
import com.digital.coffee.auth.viewmodel.AuthViewModel
import com.digital.coffee.auth.viewmodel.AuthViewState
import com.digital.coffee.core.utils.binding.viewBinding
import com.digital.coffee.core.utils.di.InjectUtils
import com.digital.coffee.core.utils.flow.RemoteData
import com.digital.coffee.core.utils.fragment.BaseFragment
import com.digital.coffee.core.utils.fragment.hideKeyboard
import com.digital.coffee.core.utils.fragment.observeFlow
import com.google.android.material.snackbar.Snackbar

class AuthFragment : BaseFragment(R.layout.fmt_auth) {

    private val viewModel: AuthViewModel by viewModels { viewModelFactory }
    private val viewBinding: FmtAuthBinding by viewBinding(FmtAuthBinding::bind)

    private val cameraPermissionRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { result ->
            viewModel.onPermissionResult(result)
        }

    override fun onInitDependencyInjection() {
        DaggerAuthComponent
            .builder()
            .coreComponent(InjectUtils.provideCoreComponent(requireActivity().applicationContext))
            .authModule(AuthModule())
            .build()
            .inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding.bLogin.setOnClickListener {
            if (!viewModel.isCameraPermissionGranted()) {
                cameraPermissionRequest.launch(AuthViewModel.CAMERA_PERMISSION)
                return@setOnClickListener
            } else {
                permissionGranted()
            }

            hideKeyboard()
        }

        observeFlow { viewModel.events.collect(::onEvent) }
        observeFlow { viewModel.viewState.collect(::updateUiState) }
    }

    private fun onEvent(event: AuthEvent) {
        when (event) {
            is AuthEvent.CameraPermissionGranted -> {
                if (event.isGranted) {
                    permissionGranted()
                } else {
                    Snackbar.make(
                        requireView(),
                        getString(R.string.auth_camera_is_required),
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun updateUiState(state: AuthViewState) {
        when (state.token) {
            is RemoteData.Initial, RemoteData.Loading -> {}
            is RemoteData.Success -> {
                viewModel.setUserToken(state.token.value)
                Snackbar.make(
                    requireView(),
                    getString(R.string.auth_success),
                    Snackbar.LENGTH_SHORT
                ).show()
                findNavController().navigate(Uri.parse("teapot://camera"))
            }
            is RemoteData.Failure -> {}
        }
    }

    private fun permissionGranted() {
        viewModel.authUser(
            name = viewBinding.etName.text.toString(),
            phone = viewBinding.etPhone.text.toString()
        )
    }
}