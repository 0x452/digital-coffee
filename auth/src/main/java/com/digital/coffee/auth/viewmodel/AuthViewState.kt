package com.digital.coffee.auth.viewmodel

import com.digital.coffee.core.utils.flow.RemoteData

data class AuthViewState(
    val token: RemoteData<String> = RemoteData.Initial
)
