package com.digital.coffee.auth.data.model.request

data class AuthRequestModel(
    val name: String,
    val phone: String
)