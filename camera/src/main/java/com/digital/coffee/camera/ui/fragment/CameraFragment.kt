package com.digital.coffee.camera.ui.fragment

import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.navigation.fragment.findNavController
import com.digital.coffee.camera.R
import com.digital.coffee.camera.databinding.FmtCameraBinding
import com.digital.coffee.camera.ui.adapter.CameraAdapter
import com.digital.coffee.core.utils.fragment.BaseFragment
import com.digital.coffee.core.utils.binding.viewBinding
import com.digital.coffee.core.utils.coroutines.Dispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class CameraFragment : BaseFragment(R.layout.fmt_camera) {

    private val viewBinding: FmtCameraBinding by viewBinding(FmtCameraBinding::bind)

    override fun onInitDependencyInjection() {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val cameraAdapter = CameraAdapter(requireContext(), viewBinding.svCamera)
        cameraAdapter.cameraResultListener = { coffeeshopId ->
            CoroutineScope(Job() + Dispatcher.Main).launch {
                findNavController().navigate(Uri.parse("teapot://coffeeMenu/${coffeeshopId}"))
            }
        }
    }
}