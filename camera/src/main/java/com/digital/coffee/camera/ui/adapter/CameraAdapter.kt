package com.digital.coffee.camera.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.SparseArray
import android.view.SurfaceHolder
import android.view.SurfaceView
import androidx.core.util.isNotEmpty
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import timber.log.Timber

class CameraAdapter(context: Context, cameraView: SurfaceView) {
    private var detector: BarcodeDetector = BarcodeDetector.Builder(context).build()
    private var cameraSource: CameraSource = CameraSource.Builder(context, detector)
        .setAutoFocusEnabled(true)
        .build()

    var cameraResultListener: ((String) -> Unit)? = null

    private val surfaceCallback = object : SurfaceHolder.Callback {
        override fun surfaceChanged(p0: SurfaceHolder, p1: Int, p2: Int, p3: Int) {}

        @SuppressLint("MissingPermission")
        override fun surfaceCreated(surfaceHolder: SurfaceHolder) {
            try {
                cameraSource.start(surfaceHolder)
            } catch (exception: Exception) {
                Timber.e(exception)
            }
        }

        override fun surfaceDestroyed(surfaceHolder: SurfaceHolder) {
            cameraSource.stop()
        }
    }

    private val processor = object : Detector.Processor<Barcode> {
        override fun release() {}

        override fun receiveDetections(detections: Detector.Detections<Barcode>?) {
            if (detections != null && detections.detectedItems.isNotEmpty()) {
                val qrCodes: SparseArray<Barcode> = detections.detectedItems
                if (cameraResultListener != null) {
                    cameraResultListener?.invoke(qrCodes.valueAt(0).rawValue)
                }
            }
        }
    }


    init {
        cameraView.holder.addCallback(surfaceCallback)
        detector.setProcessor(processor)
    }
}