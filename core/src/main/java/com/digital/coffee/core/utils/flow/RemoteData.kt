package com.digital.coffee.core.utils.flow

sealed class RemoteData<out V> {

    object Initial : RemoteData<Nothing>()

    object Loading : RemoteData<Nothing>()

    data class Success<out V>(val value: V) : RemoteData<V>()

    data class Failure(val error: Exception) : RemoteData<Nothing>()

    val isInitial
        get() = this is Initial

    val isLoading
        get() = this is Loading

    val isSuccess
        get() = this is Success

    val isFailure
        get() = this is Failure

    val isComplete
        get() = isSuccess || isFailure

}