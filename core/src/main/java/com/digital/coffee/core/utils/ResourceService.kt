package com.digital.coffee.core.utils

import android.content.Context

class ResourceService(private val context: Context) {

    fun getString(resourceId: Int, vararg params: Any?): String =
         if (params.isNotEmpty())
            context.getString(resourceId, *params)
        else
            context.getString(resourceId)
}