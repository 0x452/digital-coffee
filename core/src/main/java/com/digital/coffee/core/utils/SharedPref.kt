package com.digital.coffee.core.utils

import android.content.Context
import android.content.SharedPreferences

class SharedPref(context: Context) {

    private companion object {
        const val FILENAME_KEY = "application"

        const val TOKEN_KEY = "token"
        const val FIREBASE_TOKEN = "firebase_token"
    }

    private var sharedPreferences: SharedPreferences =
        context.getSharedPreferences(FILENAME_KEY, Context.MODE_PRIVATE)

    fun setUserToken(token: String) {
        sharedPreferences.edit().putString(TOKEN_KEY, token).apply()
    }

    fun setFirebaseToken(token: String) {
        sharedPreferences.edit().putString(FIREBASE_TOKEN, token).apply()
    }

    fun getFirebaseToken(): String? = sharedPreferences.getString(FIREBASE_TOKEN, null)

    fun getUserToken(): String? = sharedPreferences.getString(TOKEN_KEY, null)

}