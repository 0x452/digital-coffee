package com.digital.coffee.core.di.module

import android.content.Context
import com.digital.coffee.core.utils.NetworkSettings
import com.digital.coffee.core.utils.SharedPref
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideOkHttpClient(context: Context): OkHttpClient =
        OkHttpClient()
            .newBuilder()
            .addInterceptor { interceptor ->
                val request = interceptor.request().newBuilder()
                val token = SharedPref(context).getUserToken()

                token?.let { request.header("Authorization", it) }
    
                interceptor.proceed(request.build())
            }
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()

    @Singleton
    @Provides
    fun provideRetrofitBuilder(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(NetworkSettings.buildBaseUrl())
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
}