package com.digital.coffee.core.ui.permissions

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

class PermissionHelper(private val context: Context): IPermissionHelper {

    override fun isPermissionGranted(permission: String)
        = ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
}