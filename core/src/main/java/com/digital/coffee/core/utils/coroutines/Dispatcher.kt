package com.digital.coffee.core.utils.coroutines

import androidx.annotation.VisibleForTesting
import kotlinx.coroutines.CoroutineDispatcher

object Dispatcher {

    private var dispatcherProvider: IDispatcherProvider = DispatcherProvider()

    val IO: CoroutineDispatcher get() = dispatcherProvider.IO
    val Main: CoroutineDispatcher get() = dispatcherProvider.Main
    val Default: CoroutineDispatcher get() = dispatcherProvider.Default
    val Unconfined: CoroutineDispatcher get() = dispatcherProvider.Unconfined

    @VisibleForTesting
    fun setDispatcherProvider(dispatcherProvider: IDispatcherProvider) {
        Dispatcher.dispatcherProvider = dispatcherProvider
    }

}