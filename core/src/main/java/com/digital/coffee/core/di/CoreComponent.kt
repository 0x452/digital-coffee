package com.digital.coffee.core.di

import android.content.Context
import com.digital.coffee.core.di.module.CoreModule
import com.digital.coffee.core.di.module.NetworkModule
import com.digital.coffee.core.di.module.SharedPrefModule
import com.digital.coffee.core.ui.permissions.IPermissionHelper
import com.digital.coffee.core.utils.ResourceService
import com.digital.coffee.core.utils.SharedPref
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Component(
    modules = [
        CoreModule::class,
        NetworkModule::class,
        SharedPrefModule::class
    ]
)

@Singleton
interface CoreComponent {

    fun context(): Context

    fun retrofit(): Retrofit

    fun sharedPref(): SharedPref

    fun resourceService(): ResourceService

    fun permissionHelper(): IPermissionHelper
}