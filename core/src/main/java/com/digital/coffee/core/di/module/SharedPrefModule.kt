package com.digital.coffee.core.di.module

import android.content.Context
import com.digital.coffee.core.utils.SharedPref
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SharedPrefModule {

    @Provides
    @Singleton
    fun provideSharedPref(context: Context)
        = SharedPref(context)
}