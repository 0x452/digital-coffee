package com.digital.coffee.core.utils.fragment

import android.content.Context
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

abstract class BaseFragment(@LayoutRes layoutId: Int) : Fragment(layoutId) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    abstract fun onInitDependencyInjection()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onInitDependencyInjection()
    }
}