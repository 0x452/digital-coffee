package com.digital.coffee.core.utils.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.digital.coffee.core.utils.flow.EventFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

abstract class BaseViewModel<State, Event> : ViewModel() {

    abstract val initalState: State
    protected val _viewState: MutableStateFlow<State> by lazy { MutableStateFlow(initalState) }

    val viewState: StateFlow<State>
        get() = _viewState.asStateFlow()

    val events = EventFlow<Event>()

    fun postEvent(event: Event) {
        viewModelScope.launch {
            events.emit(event)
        }
    }
}