package com.digital.coffee.core.utils

class BundleKeys {
    companion object {
        const val COFFEESHOP_ID_KEY = "coffeeshopId"
        const val ORDER_ID_KEY = "orderId"
    }
}