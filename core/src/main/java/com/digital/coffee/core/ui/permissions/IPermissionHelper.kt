package com.digital.coffee.core.ui.permissions

interface IPermissionHelper {
    fun isPermissionGranted(permission: String): Boolean
}