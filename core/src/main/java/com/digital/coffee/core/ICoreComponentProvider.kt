package com.digital.coffee.core

import com.digital.coffee.core.di.CoreComponent

interface ICoreComponentProvider {

    fun provideCoreComponent(): CoreComponent
}