package com.digital.coffee.core.utils.di

import android.content.Context
import com.digital.coffee.core.ICoreComponentProvider
import com.digital.coffee.core.di.CoreComponent

object InjectUtils {

    fun provideCoreComponent(applicationContext: Context): CoreComponent {
        return if (applicationContext is ICoreComponentProvider) {
            (applicationContext as ICoreComponentProvider).provideCoreComponent()
        } else {
            throw IllegalStateException("Provide the application context which implement CoreComponentProvider")
        }
    }

}