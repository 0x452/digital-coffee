package com.digital.coffee.core.utils.coroutines

import kotlinx.coroutines.CoroutineDispatcher

interface IDispatcherProvider {
    val IO: CoroutineDispatcher
    val Main: CoroutineDispatcher
    val Default: CoroutineDispatcher
    val Unconfined: CoroutineDispatcher
}