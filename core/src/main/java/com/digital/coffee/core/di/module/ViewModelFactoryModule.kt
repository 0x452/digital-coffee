package com.digital.coffee.core.di.module

import androidx.lifecycle.ViewModelProvider
import com.digital.coffee.core.utils.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}