package com.digital.coffee.core.di.module

import android.app.Application
import android.content.Context
import com.digital.coffee.core.ui.permissions.IPermissionHelper
import com.digital.coffee.core.ui.permissions.PermissionHelper
import com.digital.coffee.core.utils.ResourceService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CoreModule(private val application: Application) {

    @Singleton
    @Provides
    fun provideContext(): Context = application

    @Singleton
    @Provides
    fun provideResourcesService(context: Context): ResourceService = ResourceService(context)

    @Singleton
    @Provides
    fun providePermissionHelper(context: Context): IPermissionHelper = PermissionHelper(context)
}