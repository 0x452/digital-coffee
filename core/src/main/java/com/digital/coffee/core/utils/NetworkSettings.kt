package com.digital.coffee.core.utils

import com.digital.coffee.core.BuildConfig

object NetworkSettings {

    private const val BASE_DEV_SERVER = "https://api.dev.digitalcoffee.app"
    private const val BASE_PROD_SERVER = "https://api.digitalcoffee.app"

    fun buildBaseUrl(): String =
        if (BuildConfig.DEBUG) BASE_DEV_SERVER else BASE_PROD_SERVER

}