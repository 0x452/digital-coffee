package com.digital.coffee.di

import com.digital.coffee.DigitalCoffee
import com.digital.coffee.core.di.CoreComponent
import com.digital.coffee.core.di.scope.AppScope
import com.digital.coffee.di.module.AppModule
import com.digital.coffee.ui.activity.MainActivity
import dagger.Component

@AppScope
@Component(
    dependencies = [
        CoreComponent::class
    ],
    modules = [
        AppModule::class
    ]
)

interface AppComponent {

    fun inject(application: DigitalCoffee)

    fun inject(activity: MainActivity)
}