package com.digital.coffee.di.module

import android.content.Context
import com.digital.coffee.DigitalCoffee
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun provideContext(application: DigitalCoffee): Context = application.applicationContext
}