package com.digital.coffee

import android.app.Application
import com.digital.coffee.core.di.CoreComponent
import com.digital.coffee.core.ICoreComponentProvider
import com.digital.coffee.core.di.DaggerCoreComponent
import com.digital.coffee.core.di.module.CoreModule
import com.digital.coffee.di.DaggerAppComponent
import com.yandex.mapkit.MapKitFactory
import timber.log.Timber

class DigitalCoffee: Application(), ICoreComponentProvider {

    lateinit var coreComponent: CoreComponent

    override fun onCreate() {
        super.onCreate()

        coreComponent = DaggerCoreComponent
            .builder()
            .coreModule(CoreModule(this))
            .build()

        DaggerAppComponent
            .builder()
            .coreComponent(coreComponent)
            .build()
            .inject(this)

        Timber.plant(Timber.DebugTree())

        MapKitFactory.setApiKey(BuildConfig.YANDEX_MAP_KEY)
    }

    override fun provideCoreComponent(): CoreComponent = coreComponent
}