package com.digital.coffee.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.os.bundleOf
import androidx.navigation.NavDeepLinkBuilder
import com.digital.coffee.R
import com.digital.coffee.core.utils.BundleKeys.Companion.COFFEESHOP_ID_KEY
import com.digital.coffee.core.utils.BundleKeys.Companion.ORDER_ID_KEY
import com.digital.coffee.core.utils.SharedPref
import com.digital.coffee.ui.activity.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber

class FirebaseMessaging : FirebaseMessagingService() {

    private companion object {
        const val CHANNEL_ID = "notifications"
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        SharedPref(baseContext).setFirebaseToken(token)
        Timber.d("Firebase token: $token")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(remoteMessage.notification?.title)
            .setContentText(remoteMessage.notification?.body)
            .setSmallIcon(R.drawable.rounded_shape)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        val pendingIntent = NavDeepLinkBuilder(applicationContext)
            .setComponentName(MainActivity::class.java)
            .setGraph(R.navigation.nav_graph)
            .setDestination(R.id.orderDetailsFragment)
            .setArguments(
                bundleOf(
                    COFFEESHOP_ID_KEY to remoteMessage.data[COFFEESHOP_ID_KEY]?.toInt(),
                    ORDER_ID_KEY to remoteMessage.data[ORDER_ID_KEY]?.toInt()
                )
            )
            .createPendingIntent()

        Timber.d(remoteMessage.data[COFFEESHOP_ID_KEY])
        builder.setContentIntent(pendingIntent)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID,
                "Hot chocolate machine",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0, builder.build())
    }
}