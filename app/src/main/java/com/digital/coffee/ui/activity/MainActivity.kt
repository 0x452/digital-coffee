package com.digital.coffee.ui.activity

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.digital.coffee.R
import com.digital.coffee.core.utils.di.InjectUtils
import com.digital.coffee.core.utils.SharedPref
import com.digital.coffee.di.DaggerAppComponent
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var sharedPref: SharedPref

    private fun inject() {
        DaggerAppComponent
            .builder()
            .coreComponent(
                InjectUtils
                    .provideCoreComponent(applicationContext)
            )
            .build()
            .inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        inject()

        val navController = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)?.findNavController()
        // TODO: fix this strange way to find nav_host_fragment, but the simplest way doesn't work!
        navController?.addOnDestinationChangedListener { _, destination, _ ->
                when(destination.id) {
                    R.id.authFragment -> {
                        sharedPref.getUserToken()?.let { token ->
                            if(token.isNotEmpty()) {
                                navController.navigate(Uri.parse("teapot://camera"))
                            }
                        }
                    }
                }
            }
    }
}